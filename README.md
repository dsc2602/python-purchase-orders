Intelligent Purchasing System
=====================================================

# Set up Instructions
## Set up virtual environment
```
cd app/app
sudo virtualenv venv
source venv/bin/activate
pip3 install -r ../../requirements.txt
```
## Configure enviroment variables
```
cp env.py.example env.py
```
Set database and server parameters in env.py.
## Start database
Start the SQL database
Remove then re-create the database 'py_po' 
```
DROP database py_po;
CREATE database py_po;
USE py_po;
```

## Migrate database sample data
```
python3 migrations.py
python3 seeds.py
```
## Run app
```
python3 __init__.py
```
# Contributing
## Code of Conduct
Contributers to this project should abide by our Contributor Covenant Code of Conduct.

## Programming Guide
### Style Guide
When contributing to Python code, please follow the guidelines sets out by [PEP 8](http://pep8.org/) where appropriate.

[pycodestyle](https://github.com/PyCQA/pycodestyle) and [autopep8](https://pypi.python.org/pypi/autopep8/) are useful resources to use to aid adherence to PEP 8.

### Comments
- All classes, modules and functions should have doc strings defining the purpose, any args and returns.
- Block inline comments should be used to describe any tricky parts of code. However never describe what the pure Python code is actually doing, assume the reader knows Python.
- Use TODO comments for code that is temporary, a short-term solution, or good-enough but not perfect.
```
# TODO(kl@gmail.com): Use a "*" here for string repetition.
# TODO(Zeke) Change this to use relations.
```

### Naming Convention
Please follow the Google naming convention found here:
<https://google.github.io/styleguide/pyguide.html?showone=Naming#Naming>

### Main
Your code should always check if __name__ == '__main__' before executing your main program so that the main program is not executed when the module is imported.
```
def main():
      ...

if __name__ == '__main__':
    main()
```

### Database Queries
Database queries should always be contained in a try... finally... block to ensure the connection is always closed.

```
conn = dbConnect()

try:
    with conn.cursor() as cursor:
        # execute query and get results

finally:
    conn.close()
```

See examples here: http://pymysql.readthedocs.io/en/latest/user/examples.html
