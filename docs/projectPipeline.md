Project Development Pipeline
============================

# Features  
## V0.0.1
- Simple web app that can pull from an SQL database to display current stock levels and min stock levels
 - Simple dropdown menus to select supplier and then stock level variable
 - designed to just get us off the ground  
## V0.0.2
- add all database tables and views
- generate manual purchase order from supplier page
- generate pdf and fake email job - to log
  - email to appropriate user based on role
## V0.0.3
- auto generate purchase order from low stock report - save as draft
## V0.0.4
- email for confirmation of draft po
- notification on dashboard for pending po
## V0.0.5
- CSV import/feed to update databases
- CSV export of current data in database
## V0.0.6
- User role authentication
## V0.0.7
- Add dashboard
- add ability to configure whats in dashboard
  - based on user too
- CSV export of data from dashboard
## V0.1.0
- Release that can be applied anyway with some technical knowledge
