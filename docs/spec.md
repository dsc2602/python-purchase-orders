Purchase Order System
====================

# Overview
An intelligent purchasing system to manage purchase orders and all interactions  
with suppliers, as well as inventory monitoring and sales analysis to optimise  
stock levels. Seasonal trends and historical stock consumption are to be used to  
predict future demand.
# MVP
- Inventory database including real-time stock levels and minimum stock levels
- Supplier database including contact details, minimum order values
- Monitor inventory for low stock
- Create low stock report
- Web interface to generate PO for each supplier from low stock report
- Scheduled inventory updates via CSV feed
# Future functionality
- Sales history and data analysis including stock consumption rate, seasonal trends, top performing products
- Product pricing rules based on historical trends to optimise sales/profitability
- Suggest discounts on slow moving stock
- Create PDF/HTML purchase order to email to suppliers
- Integration with ecommerce systems inc. Linnworks, Magento, WooCommerce
- Data to be served by API to single page app
- Multiple users within company
- Consider budget, minimum order quantity, forecasted sales, pack sizes to optimise purchasing
# Technology
- Backend – Python
- Database – MySQL
- Frontend – via Flask. To be upgraded to Django API + Vue.js as a single page app?
