# encoding: utf-8
"""
migrations.py
This script only needs to be run once at initialisation of the web application
for the first time. It creates all the tables required in the database.

Created by Dan Scott on 2017-10-10.
Copyright (c) Dan Scott and Christopher Beard. All rights reserved.
"""
from db import dbConnect

def products():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS Products;
                    CREATE TABLE Products (
                        id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
                        sku VARCHAR(255) NOT NULL,
                        internal_reference VARCHAR(255),
                        name VARCHAR(255) NOT NULL,
                        barcode VARCHAR(20) NOT NULL,
                        allow_auto_order BOOLEAN NOT NULL DEFAULT 0,
                        allow_auto_min_stock BOOLEAN NOT NULL DEFAULT 0,
                        is_archived BOOLEAN NOT NULL DEFAULT 0,
                        tax_rate ENUM('T0', 'T5', 'T20') NOT NULL,
                        notes TEXT(2000)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def inventory():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS Inventory;
                    CREATE TABLE Inventory (
                        id INT AUTO_INCREMENT NOT NULL,
                        product_id INT NOT NULL,
                        location_id INT NOT NULL,
                        qty INT NOT NULL DEFAULT 0,
                        min_stock_level INT NOT NULL DEFAULT 0,
                        reorder_qty INT,
                        excluded BOOLEAN NOT NULL DEFAULT 0,
                        excluded_until DATE,

                        PRIMARY KEY (id),
                        INDEX (product_id),
                        INDEX (location_id),

                        FOREIGN KEY (product_id)
                            REFERENCES Products(id),

                        FOREIGN KEY (location_id)
                            REFERENCES Locations(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def productSuppliers():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS ProductSuppliers;
                    CREATE TABLE ProductSuppliers (
                        id INT AUTO_INCREMENT NOT NULL,
                        product_id INT NOT NULL,
                        supplier_id INT NOT NULL,
                        is_default BOOLEAN NOT NULL DEFAULT 0,
                        cost FLOAT NOT NULL DEFAULT 0,
                        box_qty INT,
                        part_number VARCHAR(255),
                        lead_time INT NOT NULL DEFAULT 0,

                        PRIMARY KEY (id),
                        INDEX (product_id),
                        INDEX (supplier_id),

                        FOREIGN KEY (product_id)
                            REFERENCES Products(id),

                        FOREIGN KEY (supplier_id)
                            REFERENCES Suppliers(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def locations():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS Locations;
                    CREATE TABLE Locations (
                        id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
                        name VARCHAR(255) NOT NULL,
                        add1 VARCHAR(255) NOT NULL,
                        add2 VARCHAR(255),
                        add3 VARCHAR(255),
                        town VARCHAR(255) NOT NULL,
                        county VARCHAR(255) NOT NULL,
                        postcode VARCHAR(255) NOT NULL,
                        country VARCHAR(255) NOT NULL,
                        phone VARCHAR(255),
                        email VARCHAR(255)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def sales():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS Sales;
                    CREATE TABLE Sales (
                        id INT AUTO_INCREMENT NOT NULL,
                        product_id INT NOT NULL,
                        date DATE NOT NULL,
                        qty_sold INT NOT NULL,

                        PRIMARY KEY (id),
                        INDEX (product_id),

                        FOREIGN KEY (product_id)
                            REFERENCES Products(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def suppliers():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS Suppliers;
                    CREATE TABLE Suppliers (
                        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        name VARCHAR(255) NOT NULL,
                        min_order_value FLOAT NOT NULL DEFAULT 0,
                        allow_auto_order BOOLEAN NOT NULL DEFAULT 0,
                        credit_terms INT,
                        credit_limit FLOAT,
                        notes TEXT(2000),
                        lead_time INT NOT NULL DEFAULT 0,
                        currency VARCHAR(255) NOT NULL
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def contacts():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS Contacts;
                    CREATE TABLE Contacts (
                        id INT AUTO_INCREMENT NOT NULL,
                        supplier_id INT NOT NULL,
                        name VARCHAR(255) NOT NULL,
                        job_title VARCHAR(255),
                        email VARCHAR(255),
                        phone VARCHAR(255),
                        is_default BOOLEAN NOT NULL DEFAULT 0,

                        PRIMARY KEY (id),
                        INDEX (supplier_id),

                        FOREIGN KEY (supplier_id)
                            REFERENCES Suppliers(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def supplierAddresses():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS SupplierAddresses;
                    CREATE TABLE SupplierAddresses (
                        id INT AUTO_INCREMENT NOT NULL,
                        supplier_id INT NOT NULL,
                        add1 VARCHAR(255) NOT NULL,
                        add2 VARCHAR(255),
                        add3 VARCHAR(255),
                        town VARCHAR(255) NOT NULL,
                        county VARCHAR(255) NOT NULL,
                        postcode VARCHAR(255) NOT NULL,
                        country VARCHAR(255) NOT NULL,
                        is_default BOOLEAN NOT NULL DEFAULT 0,
                        type ENUM('Warehouse', 'Office', 'Other'),

                        PRIMARY KEY (id),
                        INDEX (supplier_id),

                        FOREIGN KEY (supplier_id)
                            REFERENCES Suppliers(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def purchaseOrders():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS PurchaseOrders;
                    CREATE TABLE PurchaseOrders (
                        id INT AUTO_INCREMENT NOT NULL,
                        supplier_id INT NOT NULL,
                        date_ordered DATE NOT NULL,
                        date_delivery DATE,
                        order_status ENUM('Draft', 'Pending', 'Open', 'Delivered'),
                        location_id INT NOT NULL,
                        reference VARCHAR(255) NOT NULL,
                        supplier_reference VARCHAR(255),
                        created_by INT NOT NULL,
                        confirmed_by INT,
                        notes TEXT(2000),

                        PRIMARY KEY (id),
                        INDEX (supplier_id),
                        INDEX (location_id),
                        INDEX (created_by, confirmed_by),

                        FOREIGN KEY (location_id)
                            REFERENCES Locations(id),

                        FOREIGN KEY (supplier_id)
                            REFERENCES Suppliers(id),

                        FOREIGN KEY (created_by)
                            REFERENCES Users(id),

                        FOREIGN KEY (confirmed_by)
                            REFERENCES Users(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def purchaseOrderRows():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS PurchaseOrderRows;
                    CREATE TABLE PurchaseOrderRows (
                        id INT AUTO_INCREMENT NOT NULL,
                        purchase_order_id INT NOT NULL,
                        product_id INT NOT NULL,
                        qty INT NOT NULL,
                        price FLOAT NOT NULL,

                        PRIMARY KEY (id),
                        INDEX (product_id),
                        INDEX (purchase_order_id),

                        FOREIGN KEY (product_id)
                            REFERENCES Products(id),

                        FOREIGN KEY (purchase_order_id)
                            REFERENCES PurchaseOrders(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def users():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS Users;
                    CREATE TABLE Users (
                        id INT AUTO_INCREMENT NOT NULL,
                        email VARCHAR(255) NOT NULL,
                        password VARCHAR(255) NOT NULL,
                        role_id INT NOT NULL,
                        is_admin BOOLEAN NOT NULL DEFAULT 0,
                        is_super_admin BOOLEAN NOT NULL DEFAULT 0,

                        PRIMARY KEY (id),
                        INDEX (role_id),

                        FOREIGN KEY (role_id)
                            REFERENCES Roles(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def roles():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS Roles;
                    CREATE TABLE Roles (
                        id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
                        name VARCHAR(255) NOT NULL,
                        can_confirm_order BOOLEAN NOT NULL DEFAULT 0,
                        max_allowed_price FLOAT NOT NULL DEFAULT 0
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

def allowedSuppliers():
    conn = dbConnect()

    try:
        with conn.cursor() as cursor:
            query = """
                    DROP TABLE IF EXISTS AllowedSuppliers;
                    CREATE TABLE AllowedSuppliers (
                        id INT AUTO_INCREMENT NOT NULL,
                        user_id INT NOT NULL,
                        supplier_id INT NOT NULL,
                        receive_email BOOLEAN NOT NULL DEFAULT 0,

                        PRIMARY KEY (id),
                        INDEX (user_id),
                        INDEX (supplier_id),

                        FOREIGN KEY (user_id)
                            REFERENCES Users(id),

                        FOREIGN KEY (supplier_id)
                            REFERENCES Suppliers(id)
                    );
                    """
            cursor.execute(query)

        conn.commit()

    finally:
        conn.close()

if __name__ == "__main__":
    # Care of the order that these functions are called in needs to be take
    # as can't use FOREIGN KEY before parent table is created
    products()
    locations()
    inventory()
    suppliers()
    productSuppliers()
    sales()
    contacts()
    supplierAddresses()
    roles()
    users()
    purchaseOrders()
    purchaseOrderRows()
    allowedSuppliers()
