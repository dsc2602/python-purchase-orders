# encoding: utf-8
"""
product.py
This is the script which pulls info about products from the SQL db

Created by Dan Scott on 2017-10-10.
Copyright (c) Dan Scott and Christopher Beard. All rights reserved.
"""
import logging
from db import dbConnect

class Product(object):
    def __init__(self, id):
        """
        Create a product object by ID from database.

        Args:
            id (int): product ID

        Returns:
            None
        """

        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "SELECT * FROM Products WHERE id = {};".format(id)
                results = cursor.execute(query)

                if results > 0:
                    p = cursor.fetchone()

                    for key in p:
                        setattr(self, key, p[key])


        finally:
            conn.close()


    def getSupplier(self):
        """
        Get supplier for the product object.

        Args:


        Returns:
            None
        """
        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "SELECT * FROM ProductSuppliers ps INNER JOIN Suppliers s ON ps.supplier_id = s.id WHERE ps.product_id = {};".format(int(self.supplier_id))
                cursor.execute(query)
                s = cursor.fetchone()

        finally:
            conn.close()

        self.supplier = s

    @staticmethod
    def getAll():
        """
        Static method to get full product list.

        Args:


        Returns:
           list: List of products
        """

        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "SELECT * FROM Products;"
                cursor.execute(query)
                p = cursor.fetchall()
            logging.debug('All Products fetched from database')
        finally:
            conn.close()

        return p

if __name__ == "__main__":
    pass
