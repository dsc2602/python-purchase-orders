from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SelectField
from wtforms.validators import DataRequired

class productDetails(FlaskForm):
    # TODO: fix for new db structure
    sku = StringField('SKU', validators=[DataRequired()])
    name = StringField('Name', validators=[DataRequired()])
    description = TextAreaField('Description')
    cost = StringField('Cost', validators=[DataRequired()])
    inStock = StringField('Stock', validators=[DataRequired()])
    minStock = StringField('Minimum stock level', validators=[DataRequired()])
    supplier = SelectField('Supplier', choices = [(1, 'Supplier 1'), (2, 'Supplier 2')], validators=[DataRequired()])