# encoding: utf-8
"""
supplier.py
This is the script which pulls info about suppliers from the SQL db

Created by Dan Scott on 2017-10-10.
Copyright (c) Dan Scott and Christopher Beard. All rights reserved.
"""
import logging
from db import dbConnect

class Supplier(object):
    def __init__(self, id):
        """
        Create a supplier object by ID from database.

        Args:
            id (int): supplier ID

        Returns:
            None
        """
        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "SELECT * FROM Suppliers WHERE id = %i;" % id
                results = cursor.execute(query)

                if results > 0:
                    s = cursor.fetchone()

                    for key in s:
                        setattr(self, key, s[key])

        finally:
            conn.close()



    def getContacts(self):
        """
        Get contact for the supplier object.

        Args:

        Returns:
            Contacts (list)
        """
        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "SELECT * FROM Contacts WHERE supplier_id = %i;" % self.id
                cursor.execute(query)
                c = cursor.fetchall()

        finally:
            conn.close()

        self.contacts = c
        return c

    def getProducts(self):
        """
        Get products for the supplier object.

        Args:

        Returns:
            products (list)
        """
        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "SELECT * FROM ProductSuppliers ps LEFT JOIN Products p ON ps.product_id = p.id WHERE ps.supplier_id = {};".format(self.id)
                cursor.execute(query)
                p = cursor.fetchall()
        finally:
            conn.close()

        self.products = p
        return p

    def getAddresses(self):
        """
        Get addresses for the supplier object.

        Args:

        Returns:
            addresses (list)
        """
        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "SELECT * FROM SupplierAddresses WHERE supplier_id = {}".format(self.id)
                cursor.execute(query)
                a = cursor.fetchall()
        finally:
            conn.close()
        
        self.addresses = a
        return a

    @staticmethod
    def getAll():
        """
        Static method to get full suppliers list.

        Args:

        Returns:
            suppliers (list)
        """
        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "SELECT * FROM Suppliers;"
                cursor.execute(query)
                s = cursor.fetchall()
            logging.debug('All suppliers fetched from database')
        finally:
            conn.close()

        return s

if __name__ == "__main__":
    pass
