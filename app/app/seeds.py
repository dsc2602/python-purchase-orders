# encoding: utf-8
"""
seeds.py
This script is for testing purpises only during the development of the web app,
it only needs to be run during testing once at initialisation of the web application
for the first time after migrations.py to fill all the databases with random data.

It can be deleted as soon as 'real' data is imported.

Created by Dan Scott on 2017-10-10.
Copyright (c) Dan Scott and Christopher Beard. All rights reserved.
"""
from faker import Faker
from db import dbConnect
import math
from random import randint, uniform

fake = Faker()

def products(count=100):
    # Products
    # -
    # id int PK autoincrement
    # sku string 
    # internal_reference string nullable
    # name string
    # barcode string nullable
    # allow_auto_order boolean default=0
    # allow_auto_min_stock boolean default=0
    # is_archived boolean default=0
    # tax_rate enum # ['T0', 'T5', 'T20']
    # notes text nullable

    for i in range(1, count + 1):
        sku = "SKU" + str(i)
        name = fake.word()
        barcode = fake.ean13()
        tax_rate = "T20"

        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "INSERT INTO Products (sku, name, barcode, tax_rate) VALUES ('{}', '{}', '{}', '{}')".format(sku, name, barcode, tax_rate)
                cursor.execute(query)

            conn.commit()
        
        finally:
            conn.close()

def inventory(count=200):
    # Inventory
    # -
    # id int PK autoincrement
    # product_id int FK >- Products.id
    # location_id int FK >- Locations.id
    # qty int default=0
    # min_stock_level int default=0
    # reorder_qty int nullable
    # excluded boolean default=0
    # excluded_until date nullable

    for i in range(1, count + 1):
        productId = math.ceil(i / 2)
        locationId = 1
        qty = randint(0, 20)
        minStockLevel = randint(0, 8)
        reorderQty = randint(0, 10)
        
        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "INSERT INTO Inventory (product_id, location_id, qty, min_stock_level, reorder_qty) VALUES ({}, {}, {}, {}, {})".format(productId, locationId, qty, minStockLevel, reorderQty)
                cursor.execute(query)
            
            conn.commit()
        
        finally:
            conn.close()



def productSuppliers(count=100):
    # ProductSuppliers
    # -
    # id int PK autoincrement
    # product_id int FK >- Products.id
    # supplier_id int FK >- Suppliers.id
    # is_default boolean
    # cost float
    # box_qty int nullable
    # part_number string nullable
    # lead_time int default=0

    for i in range(1, count + 1):
        productId = i
        supplierId = randint(1, 10)
        isDefault = 1
        cost = uniform(1, 20)
        boxQty = randint(1, 10)
        leadTime = randint(1, 20)

        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "INSERT INTO ProductSuppliers (product_id, supplier_id, is_default, cost, box_qty, lead_time) VALUES ({}, {}, {}, {}, {}, {})".format(productId, supplierId, isDefault, cost, boxQty, leadTime)
                cursor.execute(query)
            
            conn.commit()
        
        finally:
            conn.close()

def locations(count=3):
    # Locations
    # -
    # id int PK autoincrement
    # name string
    # add1 string
    # add2 string nullable
    # add3 string nullable
    # town string
    # county string
    # postcode string
    # country string
    # phone string nullable
    # email string nullable

    for i in range(1, count + 1):
        name = fake.word()
        add1 = fake.street_address()
        add2 = fake.secondary_address()
        town = fake.city()
        county = fake.state()
        postcode = fake.postcode()
        country = fake.country()
        phone = fake.phone_number()
        email = fake.email()

        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "INSERT INTO Locations (name, add1, add2, town, county, postcode, country, phone, email) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(name, add1, add2, town, county, postcode, country, phone, email)
                cursor.execute(query)
            
            conn.commit()
        
        finally:
            conn.close()    

# def sales():
    # Sales
    # -
    # id PK int autoincrement
    # product_id int FK >- Products.id
    # date date
    # qty_sold int

def suppliers(count=10):
    # Suppliers
    # -
    # id int PK autoincrement
    # name string
    # min_order_value float default=0
    # allow_auto_order boolean default=0
    # allow_auto_min_stock boolean default=0
    # credit_terms int nullable
    # credit_limit float nullable
    # notes text nullable
    # lead_time int default=0
    # currency string

    for i in range(1, count + 1):
        name = fake.company()
        currency = fake.currency_code()

        conn = dbConnect()
        
        try:
            with conn.cursor() as cursor:
                query = "INSERT INTO Suppliers (name, currency) VALUES ('{}', '{}');".format(name, currency)
                cursor.execute(query)

            conn.commit()
        
        finally:
            conn.close()

def contacts(count=20):
    # Contacts
    # -
    # id int PK autoincrement
    # supplier_id int FK >- Suppliers.id
    # name string
    # job_title string nullable
    # email string nullable
    # phone string nullable
    # is_default boolean

    for i in range(1, count + 1):
        supplierId = math.ceil(i / 2)
        name = fake.name()
        jobTitle = fake.job()
        email = fake.email()
        phone = fake.phone_number()
        isDefault = count % 2 == 0

        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "INSERT INTO Contacts (supplier_id, name, job_title, email, phone, is_default) VALUES ({}, '{}', '{}', '{}', '{}', {})".format(supplierId, name, jobTitle, email, phone, isDefault)
                cursor.execute(query)

            conn.commit()
        
        finally:
            conn.close()

def supplierAddresses(count=10):
    # SupplierAddresses
    # -
    # id int PK autoincrement
    # supplier_id int FK >- Suppliers.id
    # add1 string
    # add2 string nullable
    # add3 string nullable
    # town string
    # county string
    # postcode string
    # country string
    # is_default boolean
    # type enum # ['Warehouse', 'Office', 'Other']

    for i in range(1, count + 1):
        supplierId = i
        add1 = fake.street_address()
        add2 = fake.secondary_address()
        town = fake.city()
        county = fake.state()
        postcode = fake.postcode()
        country = fake.country()
        isDefault = 1
        addType = "Warehouse"

        conn = dbConnect()

        try:
            with conn.cursor() as cursor:
                query = "INSERT INTO SupplierAddresses (supplier_id, add1, add2, town, county, postcode, country, is_default, type) VALUES ({}, '{}', '{}', '{}', '{}', '{}', '{}', {}, '{}')".format(supplierId, add1, add2, town, county, postcode, country, isDefault, addType)
                cursor.execute(query)
            
            conn.commit()
        
        finally:
            conn.close()   

# def purchaseOrders():
    # PurchaseOrders
    # -
    # id int PK autoincrement
    # supplier_id int FK >- Suppliers.id
    # date_ordered date
    # date_delivery date
    # order_status enum # ['Draft', 'Pending', 'Open', 'Delivered']
    # location_id int FK >- Locations.id
    # reference string
    # supplier_reference string nullable
    # created_by int FK >- Users.id
    # confirmed_by int FK >- Users.id nullable
    # notes text nullable

# def purchaseOrderRows():
    # PurchaseOrderRows
    # -
    # id int PK autoincrement
    # purchase_order_id int FK >- PurchaseOrders.id
    # product_id int FK >- Products.id
    # qty int default=0
    # price float

# def users():
    # Users
    # -
    # id int PK autoincrement
    # email string
    # password string
    # role_id int FK >- Roles.id
    # is_admin boolean
    # is_super_admin boolean

# def roles():
    # Roles
    # -
    # id int PK autoincrement
    # name string
    # can_confirm_order boolean
    # max_allowed_price float

# def allowedSuppliers():
    # AllowedSuppliers
    # -
    # id int PK autoincrement
    # user_id int FK >- Users.id
    # supplier_id int FK >- Suppliers.id
    # receive_email boolean

if __name__ == "__main__":
    products()
    locations()
    inventory()
    suppliers()
    productSuppliers()
    contacts()
    supplierAddresses()
