# encoding: utf-8
"""
__init__.py
This is the main python script which is reposible for running the
Intelligent Purchasing  System web app.

Licensed under the MIT license

Created by Dan Scott on 2017-10-10.
Copyright (c) Dan Scott and Christopher Beard. All rights reserved.
"""
import env
import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, render_template, request, redirect, flash
from models.supplier import Supplier
from models.product import Product
import models.forms as appForms


app = Flask(__name__)
app.secret_key = "kdafjdksd9FJ12alkfo914"

@app.route('/')
def index():
    logger.debug('Index Page Selected')
    return render_template('layout/index.html')

@app.route('/suppliers')
def supplierIndex():
    logger.debug('Supplier Page Selected')
    suppliers = Supplier.getAll()

    return render_template('supplier/index.html', suppliers=suppliers)

@app.route('/supplier/<id>')
def supplierShow(id):
    logger.debug(id +' Supplier Page Selected')
    supplierId = int(id)

    supplier = Supplier(supplierId)
    
    try:
        supplier.getProducts()
        supplier.getContacts()
        supplier.getAddresses()

        return render_template('supplier/show.html', supplier=supplier)
    
    except AttributeError:
        flash('Supplier not found.', 'danger')
        return redirect('/suppliers', code=302)

@app.route('/products')
def productIndex():
    logger.debug('Products Page Selected')
    products = Product.getAll()

    return render_template('product/index.html', products=products)

@app.route('/product/<id>')
def productShow(id):
    logger.debug(id + ' Products Page Selected')
    id = int(id)

    product = Product(id)

    try:
        if request.args.get('action') == 'update':
            form = appForms.productDetails()
            return render_template('product/update.html', product=product, form=form)
       
        else:
            return render_template('product/show.html', product=product)

    except AttributeError:
        flash('Product not found.', 'danger')
        return redirect('/products', code=302)

@app.errorhandler(404)
def not_found(e):
    logger.debug('404 Error Page')
    return render_template('404.html'), 404

if __name__ == "__main__":
    # initialise logging
    logHandler = RotatingFileHandler('MainLog.log', maxBytes=20000, backupCount=2)
    logFormatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s')
    logHandler.setFormatter(logFormatter)
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.addHandler(logHandler)

    app.run(host=env.host, port=env.port)
