# encoding: utf-8
"""
db.py
This  script which holds the variables to connect to the database.

Created by Dan Scott on 2017-10-10.
Copyright (c) Dan Scott and Christopher Beard. All rights reserved.
"""
import pymysql.cursors
import env

# Connect to the database
def dbConnect():
    return pymysql.connect(host = env.db_host,
                             user = env.db_user,
                             password = env.db_pass,
                             db = env.db_name,
                             charset = 'utf8mb4',
                             cursorclass = pymysql.cursors.DictCursor)
